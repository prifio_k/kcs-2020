Function MEchoCore {
    param (
        $text,
        $fname
    )

    echo $text | out-file -Encoding ASCII -NoNewline $fname
}

Set-Alias -Name mecho -Value MEchoCore



./gradlew.bat stage


$old_vers = $(cat last_version) -as [int]
$new_vers = $old_vers + 1

echo "update from $old_vers to $new_vers"

mecho ('
FROM prifio/chgk:{0}

MAINTAINER Tolya Ash <tolya_ash@mail.ru>

COPY ./build/dist/server-0.1.1 /app
CMD /app/bin/server
' -f $old_vers) Dockerfile

docker build -t "prifio/chgk:$new_vers" .
echo "build done"
docker push "prifio/chgk:$new_vers"
echo "push done"
mecho $new_vers last_version
rm Dockerfile
echo "ALL DONE"
