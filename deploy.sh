#!/usr/bin/env bash

hostUser=ubuntu
host='54.92.203.57'
dest=$hostUser@$host

cur_vers=$(cat last_version | awk '{$1=$1;print}')
echo "last version is $cur_vers"


eval `ssh-agent -s`
ssh-add ~/.ssh/keyaws.pem

ssh $dest '
id=$(sudo docker ps -q -n 1)
echo "current docker id $id"
if [ "$id" != "" ]
then
  docker stop $id
  echo "stop done"
  docker rm $id
  echo "rm done"
fi
docker pull prifio/chgk:'"$cur_vers"'
echo "pull done"
docker run -d -p 80:8080 prifio/chgk:'"$cur_vers"'
echo "ALL DONE"
'
