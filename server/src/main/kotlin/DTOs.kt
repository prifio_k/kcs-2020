data class UserInfo(
    val password: String // best security ever
)

data class SubmitInfo(
    val time: Long,
    val questionNumber: Int,
    val answer: String,
    val userName: String,
    val isCorrect: Boolean
)

data class RequestContext(
    val curTime: Long,
    val userName: String
)


data class APIUserInfo(
    val userName: String,
    val password: String
)

interface APIUserAction {
    val user: APIUserInfo
}


data class APIOnlyUser (
    override val user: APIUserInfo
): APIUserAction

data class APISubmitAnswer(
    override val user: APIUserInfo,
    val answer: String
) : APIUserAction

data class APIStartQuestion(
    override val user: APIUserInfo,
    val questionNumber: Int,
    val defaultAnswer: String
) : APIUserAction


data class APIGetState(
    val curTime: Long,
    val questionNumber: Int,
    val questionStartTime: Long
)
