create database if not exists chgk_db;

use chgk_db;

create table if not exists Users
(
    username nvarchar(512) not null primary key,
    password nvarchar(512) not null
);

create table if not exists Submits
(
    sendBy nvarchar(512) not null,
    questionNumber int not null,
    answer nvarchar(512) not null,
    sendTime bigint not null,
    isOk boolean not null,

    constraint sendByQN unique (sendBy, questionNumber),
    foreign key (sendBy) references Users(username)
);
