import kotlinx.coroutines.*
import java.io.Writer
import java.sql.Connection
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentLinkedQueue
import java.util.concurrent.atomic.AtomicReference
import java.util.stream.Collectors
import kotlin.random.Random

typealias ActionResult = Boolean

const val QUESTION_TIMER = 63000L // 63s

class QuestionState(
        val questionNumber: Int,
        val startTime: Long,
        val defaultAnswer: Optional<String>
) {
    private val submits = ConcurrentLinkedQueue<SubmitInfo>()

    suspend fun initCallback(connect: Connection) = coroutineScope {
        launch {
            delay(QUESTION_TIMER + 10_000)
            // for every user take his last submit
            val submitsMap = submits.stream()
                    .collect(
                            Collectors.groupingBy({ it.userName }, Collectors.collectingAndThen(
                                    Collectors.maxBy { o1, o2 -> o1.time.compareTo(o2.time) })
                            { it.get() })
                    )
            while (true) {
                try {
                    connect.addSubmits(submitsMap)
                    return@launch
                } catch (e: Exception) {
                    println(e)
                }
            }
        }
    }

    fun isEnd(ctx: RequestContext): Boolean {
        return ctx.curTime - startTime > QUESTION_TIMER
    }

    fun trySubmit(ctx: RequestContext, ans: String): ActionResult {
        if (isEnd(ctx)) {
            return false
        }
        submits += SubmitInfo(ctx.curTime, questionNumber, ans, ctx.userName,
                defaultAnswer.map { ans == it }.orElse(false))
        return true
    }
}


class Game(
        private val connect: Connection
) {
    private val users = ConcurrentHashMap<String, UserInfo>()
    private val questionState = AtomicReference<Optional<QuestionState>>(Optional.empty())
    val tokenForResult = Random.Default.nextLong(0, 1_000_000_000_000_000_000).toString()

    private fun validateOrAdd(userName: String, password: String): Boolean {
        val optPwd = connect.getPassword(userName)
        if (optPwd.isPresent) {
            return password == optPwd.get()
        }
        return connect.addUser(userName, password)
    }

    fun logIn(ctx: RequestContext, password: String): ActionResult {
        if (verifyAdmin(ctx, password)) {
            return true
        }
        val x = users[ctx.userName]
        if (x == null) {
            if (validateOrAdd(ctx.userName, password)) {
                users[ctx.userName] = UserInfo(password)
                return true
            }
            return false
        }
        return x.password == password
    }

    fun verifyUser(ctx: RequestContext, password: String): Boolean {
        val user = users[ctx.userName] ?: return false
        return user.password == password
    }

    fun verifyAdmin(ctx: RequestContext, password: String): Boolean {
        return ctx.userName == "admin" && password == "58302154116"
    }

    fun getInfo(ctx: RequestContext): APIGetState {
        val qState = questionState.get()
        val (qn, st) = qState.map { it.questionNumber to it.startTime }.orElse(-1 to 0L)
        return APIGetState(ctx.curTime, qn, st)
    }

    fun submitAnswer(ctx: RequestContext, ans: String): ActionResult {
        return questionState.get().map {
            it.trySubmit(ctx, ans)
        }.orElse(false)
    }

    suspend fun startAnswer(ctx: RequestContext, questionNumber: Int, defaultAnswer: String): ActionResult {
        val optState = questionState.get()
        if (optState.map { it.isEnd(ctx) }.orElse(true)) {
            val trueDefaultAnswer = if (defaultAnswer == "") {
                Optional.empty()
            } else {
                Optional.of(defaultAnswer)
            }
            val newState = QuestionState(questionNumber, ctx.curTime, trueDefaultAnswer)
            if (questionState.compareAndSet(optState, Optional.of(newState))) {
                newState.initCallback(connect)
                return true
            }
        }
        return false
    }

    suspend fun writeResult(w: Writer) = coroutineScope {
        val submits = connect.getSubmits()
        val qns = submits.stream().map { it.questionNumber }.collect(Collectors.toSet())

        w.write("Name")
        qns.forEach { qn ->
            w.write(";")
            w.write(qn.toString())
            w.write(";c") // correct
        }
        w.write("\n")

        var pt = 0
        while (pt < submits.size) {
            val userName = submits[pt].userName
            w.write(userName)
            qns.forEach { qn ->
                w.write(";")
                if (pt < submits.size && submits[pt].userName == userName && submits[pt].questionNumber == qn) {
                    w.write(submits[pt].answer)
                    w.write(";")
                    if (submits[pt].isCorrect) {
                        w.write("1")
                    }
                    pt += 1
                } else {
                    w.write(";")
                }
            }
            w.write("\n")
        }
    }
}
