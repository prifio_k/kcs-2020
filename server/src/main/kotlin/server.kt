import io.ktor.application.*
import io.ktor.features.*
import io.ktor.html.*
import io.ktor.http.content.*
import io.ktor.request.*
import io.ktor.routing.*
import io.ktor.gson.*
import io.ktor.http.*
import io.ktor.response.*
import io.ktor.util.pipeline.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import java.sql.DriverManager


fun Application.main() {
    install(ContentNegotiation) {
        gson {
            setPrettyPrinting()
            disableHtmlEscaping()
        }
    }

    Class.forName("com.mysql.cj.jdbc.Driver");
    val bdUrl = "jdbc:mysql://test-db1.ct69fpekflz0.us-east-1.rds.amazonaws.com:3306"
    val connection = DriverManager.getConnection(bdUrl, "admin", "mypassword")
    connection.createStatement().execute("use chgk_db")
    // what about close?? mb never?
    val gs = Game(connection)

    routing {
        get("/") {
            call.respondRedirect("/index.html")
        }

        static("/") {
            resources("/")
        }

        get("/api/get") {
            val userName = call.parameters["userName"]!!
            val ctx = RequestContext(getCurrentTS(), userName)

            call.respond(gs.getInfo(ctx))
        }

        post("api/login") {
            doResponse<APIOnlyUser>(gs, { isGoodStr(user.password) && isVeryGoodStr(user.userName) }) { ctx ->
                gs.logIn(ctx, user.password)
            }
        }

        post("/api/submitAnswer") {
            doResponse<APISubmitAnswer>(gs, { isGoodStr(answer) }, isUser = true) { ctx ->
                gs.submitAnswer(ctx, answer)
            }
        }

        post("/api/startQuestion") {
            doResponse<APIStartQuestion>(gs, { isGoodStr(defaultAnswer, maybeEmpty = true) }, isAdmin = true) { ctx ->
                gs.startAnswer(ctx, questionNumber, defaultAnswer)
            }
        }

        post("api/redirectToGetResult") {
            val arg = call.receive<APIOnlyUser>()
            val ctx = RequestContext(getCurrentTS(), arg.user.userName)
            if (!gs.verifyAdmin(ctx, arg.user.password)) {
                call.respond(HttpStatusCode.Forbidden, "Only admin can erase results")
                return@post
            }
            call.respondText("/api/getResults?token=${gs.tokenForResult}")
        }

        get("/api/getResults") {
            val token = call.parameters["token"]!!
            if (token != gs.tokenForResult) {
                call.respond(HttpStatusCode.Forbidden, "Invalid token")
                return@get
            }
            call.response.header(HttpHeaders.ContentDisposition,
                    ContentDisposition.Attachment.withParameter(ContentDisposition.Parameters.FileName, "results.csv").toString())
            call.respondTextWriter { gs.writeResult(this) }
        }
    }
}

private suspend inline fun <reified T : APIUserAction> PipelineContext<Unit, ApplicationCall>.doResponse(
        gs: Game,
        verifier: T.() -> Boolean,
        isUser: Boolean = false,
        isAdmin: Boolean = false,
        action: T.(RequestContext) -> ActionResult
) {
    val arg = call.receive<T>()
    val ctx = RequestContext(getCurrentTS(), arg.user.userName)
    if ((!isUser || gs.verifyUser(ctx, arg.user.password))
            && (!isAdmin || gs.verifyAdmin(ctx, arg.user.password))
            && verifier(arg)
    ) {
        call.respond(action(arg, ctx))
    } else {
        call.respond(false)
    }
}
