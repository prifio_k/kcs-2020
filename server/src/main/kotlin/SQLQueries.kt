import org.sqlite.SQLiteException
import java.sql.Connection
import java.sql.SQLException
import java.util.*
import java.util.stream.Collectors

fun Connection.getPassword(username: String): Optional<String> {
    val stmt = createStatement()
    val rs = stmt.executeQuery("select password from Users where username='${username}'")
    if (rs.next()) {
        return Optional.of(rs.getString("password"))
    }
    return Optional.empty()
}

fun Connection.addUser(username: String, password: String): Boolean {
    val stmt = createStatement()
    try {
        stmt.execute("insert into Users (username, password) values ('${username}', '${password}')")
        return true
    } catch (e: SQLException) {
        println(e)
        return false
    }
}

fun Connection.addSubmits(submits: Map<String, SubmitInfo>) {
    if (submits.isEmpty()) {
        return
    }
    val stmt = createStatement()
    val values = submits.values.stream()
        .map { "('${it.userName}', ${it.questionNumber}, '${it.answer}', ${it.time}, ${it.isCorrect})" }
        .collect(Collectors.joining(", "))
    stmt.execute("replace into Submits (sendBy, questionNumber, answer, sendTime, isOk) values ${values}")
}

fun Connection.getSubmits(): List<SubmitInfo> {
    val stmt = createStatement()
    val rs = stmt.executeQuery("select * from Submits order by sendBy, questionNumber")
    val ans = mutableListOf<SubmitInfo>()
    while (rs.next()) {
        ans += SubmitInfo(rs.getLong("sendTime"), rs.getInt("questionNumber"),
            rs.getString("answer"), rs.getString("sendBy"), rs.getBoolean("isOk"))
    }
    return ans
}
