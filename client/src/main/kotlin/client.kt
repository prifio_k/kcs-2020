import kotlinx.browser.window
import kotlinx.coroutines.*
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.w3c.dom.*
import org.w3c.xhr.BLOB
import org.w3c.xhr.XMLHttpRequest
import org.w3c.xhr.XMLHttpRequestResponseType
import kotlin.js.Date
import kotlin.math.max

const val QUESTION_TIMER = 63000L // 63s


private fun viewLogInHint(s: String) {
    val hint = select(".login__hint-p") as HTMLParagraphElement
    hint.innerHTML = s
    hint.style.visibility = "visible"
}


private fun setTime(time: Int) {
    val m = time / 60
    val s = time % 60

    val cls = if (isAdmin) ".admin" else ".client"
    val mp = select("${cls} .timer__mins") as HTMLParagraphElement
    val sp = select("${cls} .timer__secs") as HTMLParagraphElement

    mp.innerHTML = m.toString()
    sp.innerHTML = (if (s < 10) "0" else "") + s.toString()
}

var isAdmin = false
var userName = ""
var password = ""

var isQuestionGoing = false
var lastGet = APIGetState(1_000_000, -2, 0)

suspend fun timerCycle(state: APIGetState, localTS: Double) {
    val timeLeft = max(state.questionStartTime + QUESTION_TIMER -
            ((Date.now() - localTS).toLong() + state.curTime), 0) / 1000
    setTime(timeLeft.toInt())
    if (timeLeft > 0) {
        delay(50)
        GlobalScope.launch { timerCycle(state, localTS) }
    }
}

suspend fun getCycle() {
    val http = sendGet("/api/get?userName=${encodeURIComponent(userName)}")
    if (http.status.toInt() != 200) {
        console.log(http.status)
    } else {
        val prevState = lastGet
        lastGet = Json.decodeFromString(http.responseText)

        val wasQuestionGoing = isQuestionGoing
        isQuestionGoing = lastGet.curTime - lastGet.questionStartTime <= QUESTION_TIMER

        if (!wasQuestionGoing && isQuestionGoing) {
            GlobalScope.launch { timerCycle(lastGet, Date.now()) }
        }
        if (!isQuestionGoing) {
            setTime(0)
        }

        if (!isAdmin) {
            val qnp = select(".client .question-number") as HTMLDivElement
            qnp.innerHTML = "Question number ${lastGet.questionNumber}"
            val ansInp = select(".answer") as HTMLInputElement
            val submitButton = select(".submit-button") as HTMLButtonElement
            if (!wasQuestionGoing && isQuestionGoing) {
                ansInp.removeAttribute("readonly")
                submitButton.disabled = false
            } else if (wasQuestionGoing && !isQuestionGoing) {
                ansInp.setAttribute("readonly", "")
                ansInp.value = ""
                submitButton.disabled = true
            }
            (select(".client") as HTMLDivElement).show()
        } else {
            val qnp = select(".admin .question-number") as HTMLDivElement
            qnp.innerHTML = "Question number ${lastGet.questionNumber}"

            val qnInp = select(".admin__new-question-number") as HTMLInputElement
            val defAnsInp = select(".admin__new-question-default") as HTMLInputElement
            val launchButton = select(".launch-button") as HTMLButtonElement
            if (wasQuestionGoing && !isQuestionGoing) {
                qnInp.removeAttribute("readonly")
                defAnsInp.removeAttribute("readonly")
                qnInp.value = (lastGet.questionNumber + 1).toString()
                defAnsInp.value = ""
                launchButton.disabled = false
            } else if (!wasQuestionGoing && isQuestionGoing) {
                qnInp.setAttribute("readonly", "")
                defAnsInp.setAttribute("readonly", "")
                launchButton.disabled = true
            }
            (select(".admin") as HTMLDivElement).show()
        }
    }

    delay(500)
    GlobalScope.launch { getCycle() }
}

fun launchCycle() {
    GlobalScope.launch { getCycle() }
}

fun main() {
    window.onload = {
        select(".login__button").addEventListener("click", {
            val inpName = select(".login__input-name") as HTMLInputElement
            val inpPwd = select(".login__input-password") as HTMLInputElement

            val curUserName = inpName.value
            val curPassword = inpPwd.value
            if (!inpName.checkValidity() || !inpPwd.checkValidity()) {
                viewLogInHint("Invalid characters used")
            }
            GlobalScope.launch {
                val http = sendPost("/api/login", APIOnlyUser(APIUserInfo(curUserName, curPassword)))
                if (http.status.toInt() == 200) {
                    if (http.responseText == "true") {
                        userName = curUserName
                        password = curPassword
                        if (curUserName == "admin") {
                            isAdmin = true
                        }
                        (select(".login") as HTMLDivElement).hidden = true
                        launchCycle()
                    } else {
                        viewLogInHint("Invalid username or password")
                    }
                } else {
                    viewLogInHint("Error: ${http.status}")
                }
            }
        })
        select(".submit-button").addEventListener("click", {
            val inp = select(".answer") as HTMLInputElement
            val feedback = select(".feedback-text") as HTMLParagraphElement
            val answer = inp.value
            if (inp.checkValidity() && answer != "") {
                val roller = (select(".lds-roller")) as HTMLDivElement
                roller.show()
                GlobalScope.launch {
                    val http = sendPost("/api/submitAnswer", APISubmitAnswer(APIUserInfo(userName, password), answer))
                    if (http.status.toInt() == 200 && http.responseText == "true") {
                        roller.hidden = true
                    }
                    feedback.innerHTML = "Answer accepted"
                    feedback.show()
                }
            } else {
                feedback.innerHTML = "Invalid answer format"
                feedback.show()
            }
        })
        select(".answer").addEventListener("input", {
            (select(".feedback-text") as HTMLParagraphElement).hide()
        })
        select(".launch-button").addEventListener("click", {
            val qnInp = select(".admin__new-question-number") as HTMLInputElement
            val defAnsInp = select(".admin__new-question-default") as HTMLInputElement
            val number = qnInp.value.toIntOrNull()
            if (number == null) {
                console.log("invalid number format")
                return@addEventListener
            }
            if (!defAnsInp.checkValidity()) {
                console.log("Invalid default answer format")
                return@addEventListener
            }
            GlobalScope.launch {
                val http = sendPost("/api/startQuestion",
                        APIStartQuestion(APIUserInfo(userName, password), number, defAnsInp.value))
                if (http.status.toInt() != 200 || http.responseText == "false") {
                    console.log("Cannot start question ${http.status}")
                }
            }
        })
        select(".results-button").addEventListener("click", {
            GlobalScope.launch {
                val http = sendPost("/api/redirectToGetResult", APIOnlyUser(APIUserInfo(userName, password)))
                if (http.status.toInt() == 200) {
                    val link = select(".costil") as HTMLAnchorElement
                    link.href = http.responseText
                    link.click()
                } else {
                    console.log(http.status)
                }
            }
        })
    }
}

external fun encodeURIComponent(s: String): String
