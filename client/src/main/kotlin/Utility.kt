import kotlinx.browser.document
import kotlinx.coroutines.Job
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.w3c.dom.Element
import org.w3c.dom.HTMLElement
import org.w3c.dom.css.ElementCSSInlineStyle
import org.w3c.xhr.XMLHttpRequest

suspend inline fun sendGet(url: String): XMLHttpRequest {
    val http = XMLHttpRequest()
    http.open("GET", url)
    val job = Job()
    http.onload = { job.complete() }
    http.send()
    job.join()
    return http
}

suspend inline fun <reified T> sendPost(url: String, x: T): XMLHttpRequest {
    val http = XMLHttpRequest()
    http.open("POST", url)
    http.setRequestHeader("Content-Type", "application/json;charset=UTF-8")
    val s = Json.encodeToString(x)
    val job = Job()
    http.onload = { job.complete() }
    http.send(s)
    job.join()
    return http
}

fun HTMLElement.show() {
    style.visibility = "visible"
}

fun HTMLElement.hide() {
    style.visibility = "hidden"
}

fun select(s: String): Element {
    return document.querySelector(s)!!
}
