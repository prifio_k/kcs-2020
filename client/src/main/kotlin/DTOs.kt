import kotlinx.serialization.Serializable

@Serializable
data class APIUserInfo(
        val userName: String,
        val password: String
)

interface APIUserAction {
    val user: APIUserInfo
}


@Serializable
data class APIOnlyUser (
        override val user: APIUserInfo
): APIUserAction

@Serializable
data class APISubmitAnswer(
        override val user: APIUserInfo,
        val answer: String
) : APIUserAction

@Serializable
data class APIStartQuestion(
        override val user: APIUserInfo,
        val questionNumber: Int,
        val defaultAnswer: String
) : APIUserAction


@Serializable
data class APIGetState(
        val curTime: Long,
        val questionNumber: Int,
        val questionStartTime: Long
)
